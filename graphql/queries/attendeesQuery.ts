import { gql } from '@apollo/client'

const attendeesQuery = gql`
  query Attendees (
    $eventId: ID!
    $first: Int
  ) {
    viewer {
      event(id: $eventId) {
        attendees(first: $first) {
          totalCount
          nodes {
            id
            name
            status
            firstName
            lastName
            email
            jobPosition
            company
            phone
          }
        }
      }
    }
  }
`;

export default attendeesQuery;
