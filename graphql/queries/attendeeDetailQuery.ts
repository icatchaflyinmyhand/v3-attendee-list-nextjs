import { gql } from '@apollo/client'

const attendeeDetailQuery = gql`
  query Attendees (
    $eventId: ID!
    $attendeeId: ID!
  ) {
    viewer {
      event(id: $eventId) {
        attendee(id: $attendeeId) {
          id
          name
          firstName
          lastName
          email
          secondaryEmails {
            nodes {
              email
            }
          }
          phone
          company
        }
      }
    }
  }
`;

export default attendeeDetailQuery;
