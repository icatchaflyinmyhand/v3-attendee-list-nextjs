import { combineReducers } from 'redux';

import attendeeReducer from './attendee';

export default combineReducers({
  attendees: attendeeReducer,
});
