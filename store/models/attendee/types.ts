import { schema } from 'normalizr';
import merge from 'lodash/merge';

export enum AttendeeFetchedLevel {
  Preview = 'PREVIEW',
  Details = 'DETAILS',
}

export interface Attendee {
  id: string;
  name: string;
  email: string;
  firstName: string;
  lastName: string;
  company: string;
  fetchedLevel: AttendeeFetchedLevel;
}

export const AttendeeEntity = new schema.Entity('attendees', {}, {
  mergeStrategy(first, second) {
    return merge(first, second);
  },
});
