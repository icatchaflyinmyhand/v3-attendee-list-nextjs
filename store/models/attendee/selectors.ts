import { createEntityAdapter, createSelector } from '@reduxjs/toolkit';
import { Attendee } from './types';


export const attendeeAdapter = createEntityAdapter<Attendee>();

export const {
  selectIds: selectAttendeeIds,
  selectEntities: selectAttendeeEntities,
  selectAll: selectAllAttendees,
  selectById: selectAttendeeById,
  selectTotal: selectTotalAttendees,
} = attendeeAdapter.getSelectors((state: any) => state.attendees);

export const selectAttendeePageInfo = createSelector(
  (state: any) => state.attendees,
  (attendees) => attendees.pageInfo,
);
