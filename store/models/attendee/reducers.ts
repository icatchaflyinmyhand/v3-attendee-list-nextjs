import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { normalize } from 'normalizr';

import { attendeeAdapter, selectAttendeePageInfo } from './selectors';

import { AttendeeEntity, Attendee } from './types';
import { initializeApollo } from 'lib/apolloClient/client';
import attendeesQuery from 'graphql/queries/attendeesQuery';

const apolloClient = initializeApollo();

export const fetchAttendees = createAsyncThunk(
  'attendee/fetchAttendees',
  async (eventId: string, {
    getState,
  }) => {
    const state = getState();
    if (eventId) {
      const pageInfo = selectAttendeePageInfo(state);
      const result = await apolloClient.query({
        query: attendeesQuery,
        variables: {
          eventId,
          first: pageInfo.limit,
        },
      }).then(({ data }) => data);
      const {
        viewer: {
          event: {
            attendees: {
              nodes,
              totalCount,
            }
          }
        }
      } = result;
      let totalCountNumber;
      if (pageInfo.totalCount === 0 ) {
        totalCountNumber = (pageInfo.limit * pageInfo.currentPage) + totalCount;
      } else {
        totalCountNumber = pageInfo.totalCount;
      }
      const { entities: { attendees } } = normalize<Attendee>(nodes, [AttendeeEntity]);
      return {
        attendees,
        pageInfo: {
          ...pageInfo,
          totalCount: totalCountNumber,
        },
      }
    }
  }
)

// export const fetchAttendeeDetail = createAsyncThunk(
//   'attendee/fetchAttendeeDetail',
//   async ({
//     eventId,
//     attendeeId,
//   }, {
//     getState,
//   }) => {

//   }
// )

const initialState = attendeeAdapter.getInitialState({
  pageInfo: {
    limit: 20,
    currentPage: 0,
    totalCount: 0,
  },
});

export const attendeeSlice = createSlice({
  name: 'attendees',
  initialState: initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAttendees.fulfilled, (state: any, action) => {
      if (action.payload) {
        attendeeAdapter.setAll(state, action.payload.attendees);
        state.pageInfo = action.payload.pageInfo;
      }
    });
  },
});

export default attendeeSlice.reducer;
