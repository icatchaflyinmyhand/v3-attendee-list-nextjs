import { createContext, ReactNode, useState } from 'react';

interface IEventContext {
  id: string;
  locale: string;
  setId(id: string): void;
  setLocale(locale: string): void;
}

export const EventContext = createContext<IEventContext>(null!);

interface EventProviderProps {
  children: ReactNode;
}

export const EventProvider = ({ children }: EventProviderProps) => {
  const [id, setId] = useState<string>();
  const [locale, setLocale] = useState<string>();

  <EventContext.Provider
    value={{
      id,
      locale,
      setId,
      setLocale,
    }}
  >
    {children}
  </EventContext.Provider>
};

