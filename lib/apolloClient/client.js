import { useMemo } from 'react';
import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import 'isomorphic-fetch';
import { createTransformerLink } from 'apollo-client-transform';
import merge from 'lodash/merge';

import transformersMapping from './transformersMapping';
import dataIdFromObject from './relayStyledDataIdFromObject';
import createVariableSerializationLink from './createVariableSerializationLink';
import createHttpLink from './createHttpLink';
import serializersMapping from './serializersMapping';

const xtraGraphQLEndpointURL = process.env.NEXT_PUBLIC_XTRA_GRAPH_ENDPOINT;

const authLink = setContext((_, { headers }) => {
  let token = typeof window !== 'undefined' ? localStorage.getItem('auth_token') : '';

  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});

const link = ApolloLink.from([
  createTransformerLink(transformersMapping),
  createVariableSerializationLink(serializersMapping),
  createHttpLink({ endpoint: xtraGraphQLEndpointURL })
]);

const cache = new InMemoryCache({
  dataIdFromObject,
});

const createApolloClient = () => {
  return new ApolloClient({
    link: authLink.concat(link),
    cache,
    connectToDevTools: true,
    ssr: typeof window !== 'undefined',
  });
};

let apolloClient;

export const initializeApollo = (initialState = null) => {
  const _apolloClient = apolloClient ?? createApolloClient();

  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // get hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract()

    // Merge the existing cache into data passed from getStaticProps/getServerSideProps
    const data = merge(initialState, existingCache)

    // Restore the cache with the merged data
    _apolloClient.cache.restore(data)
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
}

export const useApollo = (initialState) => {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
};

