import DatetimeTransformer from './transformers/DatetimeTransformer';

const serializers = {
  ISO8601DateTime: DatetimeTransformer,
};

export default serializers;
