import moment from 'moment';

const DatetimeTransformer = {
  parseValue(value) {
    return value ? moment(value) : value;
  },
  serializeValue(value) {
    return value ? value.toISOString() : value;
  },
};

export default DatetimeTransformer;
