import { ApolloLink, HttpLink } from '@apollo/client';
import { onError } from '@apollo/client/link/error';

// Default Http Link used by Apollo boost

function createHttpLink({ endpoint }) {
  return ApolloLink.from([
    onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors) {
        graphQLErrors.forEach(({ message, locations, path }) => {
          // eslint-disable-next-line no-console
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          );
        });
      }
      // eslint-disable-next-line no-console
      if (networkError) console.log(`[Network error]: ${networkError}`);
    }),
    new HttpLink({
      uri: endpoint,
    }),
  ]);
}

export default createHttpLink;
