import React from 'react';
import PropTypes from 'prop-types'

import { initializeApollo } from 'lib/apolloClient/client';
import attendeeDetailQuery from 'graphql/queries/attendeeDetailQuery';
import AttendeeProfile from 'components/AttendeeProfile';

const AttendeeProfilePage = ({
  attendee
}) => {
  return (
    <AttendeeProfile
      attendeeId={attendee.id}
    />
  );
};

AttendeeProfilePage.propTypes = {
  attendee: PropTypes.string.isRequired,
}

AttendeeProfilePage.getInitialProps = async ({ query }) => {
  const { eventId, attendeeId, auth_token } = query;
  const apolloClient = initializeApollo(auth_token);
  let attendeeObject = {};
  await apolloClient.query({
    query: attendeeDetailQuery,
    variables: {
      eventId,
      attendeeId,
    },
  }).then(({ data }) => {
    const { viewer: { event: { attendee } } } = data;
    attendeeObject = attendee;
  });
  return {
    attendee: attendeeObject,
  };
}

export default AttendeeProfilePage;
