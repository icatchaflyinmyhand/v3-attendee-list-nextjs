import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import Modal from 'react-modal';

import { fetchAttendees, selectAllAttendees, selectAttendeePageInfo } from 'store/models/attendee';
import AttendeeProfile from 'components/AttendeeProfile';

import ListContainer from 'components/attendee-list/ListContainer';
import { CircularProgress } from '@material-ui/core';

Modal.setAppElement("#__next");
interface AttendeeInfo {
  nodes: any,
  pageInfo: any,
  totalCount: number,
}

const AttendeeList = () => {
  const router = useRouter();
  const { eventId, auth_token } = router.query;
  const dispatch = useDispatch();
  const attendees = useSelector((state) => selectAllAttendees(state));
  const attendeePageInfo = useSelector((state) => selectAttendeePageInfo(state));
  console.log('attendees', attendees)

  useEffect(() => {
    if (auth_token && auth_token !== localStorage.getItem('auth_token')) {
      localStorage.setItem('auth_token', auth_token as string);
    }
  }, [auth_token]);

  useEffect(() => {
    dispatch(fetchAttendees(eventId as string));
  }, [eventId]);

  useEffect(() => {
    dispatch(fetchAttendees(eventId as string));
  }, [attendeePageInfo.currentPage]);

  const paginationHandler = (page) => {
    console.log(page);
  }

  const modalClose = () => {
    router.push(`/events/${eventId}/attendees`);
  }
  return (
    <div>
      <h2>Attendee List</h2>
      <ListContainer />
      <Modal
        isOpen={!!router.query.attendeeId}
        onRequestClose={modalClose}
      >
        <AttendeeProfile attendeeId={router.query.attendeeId as string} />
      </Modal>
    </div>
  );
}

export default AttendeeList;