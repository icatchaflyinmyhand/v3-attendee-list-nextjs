import { NextPage } from 'next';

const RedirectPage: NextPage = () => null;

RedirectPage.getInitialProps = ({ res }) => {
  if (res) {
    res.writeHead(302, {
      Location: 'https://www.eventxtra.com',
    });
    res.end();
  }
  return {};
};

export default RedirectPage;
