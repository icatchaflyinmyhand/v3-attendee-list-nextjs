import { AppProps } from 'next/app';
import { Provider as ReduxProvider } from 'react-redux';
import { ApolloProvider } from '@apollo/client'
import { FC } from 'react';
import { useApollo } from '../lib/apolloClient/client';
import store from '../store/store';
import '../styles/globals.css'
import 'styles/attendee-list-styles.sass'
import 'bootstrap/dist/css/bootstrap.min.css';

const AttendeeApp: FC<AppProps> = ({ Component, pageProps }) => {
  const apolloClient = useApollo(pageProps.initialApolloState)
  return (
    <ReduxProvider store={store}>
      <ApolloProvider client={apolloClient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </ReduxProvider>
  );
}

export default AttendeeApp;
