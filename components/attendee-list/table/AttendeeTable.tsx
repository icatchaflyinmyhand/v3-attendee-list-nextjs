import React from 'react'
import PropTypes from 'prop-types'
import BootstrapTable from 'react-bootstrap-table-next'
import styled from 'styled-components';

const StyledTableContainer = styled.div`
  margin-top: 20px;
`;

const EDITABLE_FIELDS = [
  {
    dataField:'status',
    text: 'status',
  },
  {
    dataField:'firstName',
    text: 'firstName',
  },
  {
    dataField:'lastName',
    text: 'lastName',
  },
  {
    dataField:'name',
    text: 'Full Name',
  },
  {
    dataField:'email',
    text: 'email',
  },
  {
    dataField:'company',
    text: 'company',
  },
  {
    dataField:'phone',
    text: 'phone',
  },
  {
    dataField:'jobPosition',
    text: 'jobPosition',
  },
  {
    dataField:'id',
    text: 'id',
  },
]

const style = {
  tableHeader: {
    flex: '0 0 auto',
    backgroundColor: '#FAFAFA',
  },
  tableBody: {
    flex: '1 1 auto',
  },
  tableColumn: {
    wordWrap: 'break-word',
    whiteSpace: 'initial',
  },
  alertMsg: {
    fontSize: '14px',
  },
}

const AttendeeTable = ({ attendees }) => {
  return (
    <StyledTableContainer>
      <BootstrapTable
        keyField='id'
        data={attendees}
        columns={EDITABLE_FIELDS}
        bordered={false}
        hover
      />
    </StyledTableContainer>
  )
}

AttendeeTable.propTypes = {
  attendees: PropTypes.array,
}

export default AttendeeTable
