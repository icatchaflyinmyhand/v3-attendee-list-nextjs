import React from 'react'
import Image from 'next/image'

 const NoFilterResultView = () => (
  <div>
    <Image src="/no-data-found.png" alt="me" width="64" height="64" />
    <h3>
      { ('attendees.no_filter_result') }
    </h3>
    { ('attendees.please_adjust_filters') }
  </div>
)

export default NoFilterResultView
