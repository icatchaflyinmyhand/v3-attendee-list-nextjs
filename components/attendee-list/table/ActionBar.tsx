import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  Dropdown,
  Image,
} from 'react-bootstrap'
import { CircularProgress } from '@material-ui/core'

// import QuickSearchField from './QuickSearchField'
// import AttendeeListController from './utilities/attendee_list_controller'
// import AttendeeBulkUpdateModal from './AttendeeBulkUpdateModal'
// import ActionBarActionDropdownButton from './ActionBarActionDropdownButton'
import ActionBarContentLoader from './ActionBarContentLoader'

const style = {
  attendeeCountContainer: {
    display: 'flex',
    alignItems: 'center',
    height: '40px',
  },
  description: {
    marginRight: '5px'
  },
  attendeeCount: {
    marginRight: '10px',
    fontSize: '18px',
    fontFamily: 'Lato',
    fontWeight: 'bold',
  },
  spinner: {
    width: '22px',
    display: 'flex',
    alignItems: 'center',
  },
}

const ActionBar = () => {
  const [loading, setLoading] = useState(true)

  if (loading) return (<ActionBarContentLoader />)
  else {
    return (
      <div id='attendee-list-action-toolbar'>
        {/* TODO Actionbar Item */}
      </div>
    )
  }
}

ActionBar.propTypes = {
  attendeeIds: PropTypes.array.isRequired,
  appliedFilters: PropTypes.object,
  firstLoading: PropTypes.bool.isRequired,
  infiniteLoading: PropTypes.bool.isRequired,
  selectedAttendeeIds: PropTypes.array.isRequired,
  attendeeIdsToBeProcessed: PropTypes.array.isRequired,
  filtersApplied: PropTypes.bool.isRequired,
  handleActionOnClick: PropTypes.func.isRequired,
  handleActionDeleteOnClick: PropTypes.func.isRequired,
  attendees: PropTypes.object.isRequired,
}

export default ActionBar
