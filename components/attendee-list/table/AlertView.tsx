import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { CSSTransition, TransitionGroup, } from 'react-transition-group'
import { Alert } from 'react-bootstrap'
import styled from 'styled-components';

const StyledAlertMsg = styled.div`
  font-size: '14px',
`;

const AlertView = () => {
  const [show, setShow] = useState(true);
  const alertMsgs = [{msg: 'Attendee list Loaded', type: 'success'}]

  return (
    <TransitionGroup>
      {
        alertMsgs?.map((alert, index) =>
          <CSSTransition
            key={index}
            classNames='alertMsgs'
            timeout={300}
          >
            <Alert
              show={show}
              className='fade-alert'
              variant={alert.type}
              onClose={() => setShow(false)}
              dismissible
            >
              <StyledAlertMsg>
                { alert.msg }
              </StyledAlertMsg>
            </Alert>
           </CSSTransition>
        )
      }
    </TransitionGroup>
  )
}

AlertView.propTypes = {

}

export default AlertView
