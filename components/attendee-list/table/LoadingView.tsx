import React from 'react'
import PropTypes from 'prop-types'
import { CircularProgress } from '@material-ui/core'

const LoadingView = ({size=80, thickness=4, className='loading'}) => (
  <CircularProgress
    className={className}
    size={size}
    thickness={thickness}
  />
)

LoadingView.propTypes = {
  className: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number,
}

export default LoadingView
