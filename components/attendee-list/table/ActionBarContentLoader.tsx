import React from 'react';
import ContentLoader from 'react-content-loader';

const ActionBarContentLoader = () => (
  <ContentLoader
    height={60}
    width={800}
    style={{
      height: '60px',
      width: '100%',
      backgroundColor: '#fafafa',
      borderBottom: '1px solid #cbcbcbaa',
    }}
    speed={2}
    preserveAspectRatio="none"
    backgroundColor="#dadada"
    foregroundColor="#ecebeb"
  >
    <rect x="20" y="14" rx="3" ry="3" width="160" height="32" />
    <rect x="200" y="14" rx="3" ry="3" width="80" height="32" />
    <rect x="300" y="14" rx="3" ry="3" width="120" height="32" />
    <rect x="440" y="14" rx="3" ry="3" width="100" height="32" />
    <rect x="560" y="14" rx="3" ry="3" width="60" height="32" />
  </ContentLoader>
);

export default ActionBarContentLoader;
