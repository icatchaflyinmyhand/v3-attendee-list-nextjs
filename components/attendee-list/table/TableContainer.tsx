import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'

import { selectAllAttendees } from 'store/models/attendee/selectors'
import ActionBar from './ActionBar'
import CellItem from './CellItem'
import AttendeeTable from './AttendeeTable'
import NoAttendeeView from './NoAttendeeView'
import NoFilterResultView from './NoFilterResultView'
import PaginationPanel from './PaginationPanel'
import LoadingView from './LoadingView'
import AlertView from './AlertView'

const style = {
  tableHeader: {
    flex: '0 0 auto',
    backgroundColor: '#FAFAFA',
  },
  tableBody: {
    flex: '1 1 auto'
  },
  tableColumn: {
    wordWrap: 'break-word',
    whiteSpace: 'initial',
  },
  alertMsg: {
    fontSize: '14px',
  }
}


const FILTER_APPLIED = false

const TableContainer = () => {
  const attendees = useSelector(selectAllAttendees);
  const [loading, setloading] = useState(true);

  setTimeout(() => { setloading(false)}, 2000);

  return (
    <div id='attendee-list-table'>
      <ActionBar />
      { loading ? <LoadingView /> : (
        <div className='attendee-list'>
          <AlertView />
          {
            attendees?.length > 0 ? (
              <>
                <AttendeeTable attendees={attendees} />
                <PaginationPanel />
              </>
            ) : (
              <div className='empty-state'>
                { FILTER_APPLIED ? (
                  <NoFilterResultView />
                ) : (
                  <NoAttendeeView />              
                ) }
              </div>
            )
          }
        </div>
      )}
    </div>
  )
}

TableContainer.propTypes = {
  // loading: PropTypes.bool.isRequired,
}

export default TableContainer
