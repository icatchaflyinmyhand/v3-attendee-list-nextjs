import React, { useState } from 'react'
import PropTypes from 'prop-types'

// TODO: consider change lib or not using lib (auto save)
// import { EditableTextField } from 'react-bootstrap-xeditable'


const CellItem = ({
  name,
  updateUrl,
  value,
  isCustomField,
  customFields,
  updateAlertMsg,
  options,
  type,
  answerId,
}) => {
  const [loading, setLoading] = useState(false)

  const handleUpdate = () => {
    console.log('updated!')
  }

  if (loading) {
    return (
      <i className="fa fa-spinner fa-pulse fa-fw"/>
    )
  } else if (/^question_[\d]+$/.test(name)) {
    return (
      <input
        name={`answers_attributes[${name.split('_')[1]}][answer]`}
        value={value}
        // onSave={handleUpdate}
        placeholder={('general.empty')}
        defaultValue={('general.empty')}
      />
    )
  }
  return (
    <div>
      CellItem
    </div>
  )
}

CellItem.propTypes = {
  name: PropTypes.string.isRequired,
  updateUrl: PropTypes.string.isRequired,
  value: PropTypes.string,
  isCustomField: PropTypes.bool,
  customFields: PropTypes.object,
  updateAlertMsg: PropTypes.func.isRequired,
  options: PropTypes.array,
  type: PropTypes.string.isRequired,
  answerId: PropTypes.number.isRequired,
}

export default CellItem
