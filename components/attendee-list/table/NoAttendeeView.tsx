import React from 'react'
import Image from 'next/image'

const NoAttendeeView = () => (
  <div>
    <Image src="/no-attendees.png" alt="me" width="64" height="64" />
    <h3>
      { ('attendees.no_data_found') }
    </h3>
    {('attendees.please_import_attendee')}
  </div>
)

export default NoAttendeeView
