import React, { useState, useEffect } from 'react';
// import PropTypes from 'prop-types';
import { Snackbar, Button } from '@material-ui/core';
import 'es6-object-assign/auto'
import Immutable from 'immutable'

import AttendeeListSegmentContainer from 'components/attendee-list/segment/AttendeeListSegmentContainer';
import AttendeeListTableContainer from 'components/attendee-list/table/TableContainer'
import FilteringContainer from 'components/attendee-list/filtering/Container'

const BITE_SIZE = 10;
const DEFAULT_SNACKBAR_ACTION = "Reload"
const DEFAULT_SNACKBAR_DURATION = 3600000;

const AttendeeListContainer = () => {
  const [loading, setLoading] = useState(false)
  const [firstLoading, setFirstLoading] = useState(true)
  const [snackbarMsg, setSnackbarMsg] = useState('')
  const [openSnackbar, setOpenSnackbar] = useState(false)
  const [snackbarAction, setSnackbarAction] = useState(DEFAULT_SNACKBAR_ACTION)
  const [snackbarDuration, setSnackbarDuration] = useState(DEFAULT_SNACKBAR_DURATION)
  const [quickSearchKey, setQuickSearchKey] = useState('')
  const [showLayouts, setShowLayouts] = useState(true)
  const [infiniteLoading, setInfiniteLoading] = useState(true)

  const handleSnackbarActionClick = () => {
    switch (snackbarAction) {
      case 'Reload':
        window.location.reload()
        break;
      case 'Dismiss':
        setOpenSnackbar(false)
        break;
      default:
        break;
    }
  }

  return (
    <>
      <h3>Test Gitlab account</h3>
      <AttendeeListSegmentContainer
        firstLoading={firstLoading}
        infiniteLoading={infiniteLoading}
        appliedFilters={null}
      />
      <div id='attendee-list-table-container'>
        <FilteringContainer />
        <AttendeeListTableContainer />
      </div>
      <Snackbar
        open={openSnackbar}
        message={snackbarMsg}
        autoHideDuration={snackbarDuration}
        onClose={() => setOpenSnackbar(false)}
        ContentProps={{
          style: {
            fontSize: '14px'
          }
        }}
        action={[
          <Button
            key={'reload'}
            color="secondary"
            size='small'
            style={{fontSize: '14px'}}
            onClick={handleSnackbarActionClick}
          >
            {snackbarAction}
          </Button>
        ]}
      />
    </>
  )
}

export default AttendeeListContainer;