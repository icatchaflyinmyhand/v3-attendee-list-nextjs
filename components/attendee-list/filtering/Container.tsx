import React from 'react'
import PropTypes from 'prop-types'
import ContentLoader from 'react-content-loader'
import styled from 'styled-components';

const StyledFilterContainer = styled.div`
  display: flex;
  align-items: stretch;
  width: 230px;
`;

const FilteringContainer = props => {
  return (
    <StyledFilterContainer>
      <ContentLoader
        height={120}
        width={230}
        speed={2}
        preserveAspectRatio='none'
        backgroundColor="#dadada"
        foregroundColor="#ecebeb"
      >
        <rect x="20" y="26" rx="3" ry="3" width="150" height="19" />
        <rect x="20" y="55" rx="3" ry="3" width="100" height="15" />
        <rect x="20" y="80" rx="3" ry="3" width="190" height="32" />
      </ContentLoader>
    </StyledFilterContainer>
  )
}

FilteringContainer.propTypes = {};

export default FilteringContainer
