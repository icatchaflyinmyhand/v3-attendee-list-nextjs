import React from 'react';
import PropTypes from 'prop-types';

const SegmentTitle = ({ name, attendeesCount }) => (
  <div className="segment-name">
    <span className="segment-title">
      { name }
    </span>
    <span className="attendees-count">
      { `(${attendeesCount})` }
    </span>
  </div>
);

SegmentTitle.propTypes = {
  name: PropTypes.string.isRequired,
  attendeesCount: PropTypes.number.isRequired,
};

export default SegmentTitle;
