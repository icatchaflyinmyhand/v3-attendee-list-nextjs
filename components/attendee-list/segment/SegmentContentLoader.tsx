import React from 'react';
import ContentLoader from 'react-content-loader';

const SegmentContentLoader = () => (
  <ContentLoader
    height={40}
    width={120}
    style={{
      height: '40px',
      width: '120px',
    }}
    speed={1}
    backgroundColor="#dadada"
    foregroundColor="#ecebeb"
  >
    <rect x="18" y="9" rx="3" ry="3" width="84" height="22" />
  </ContentLoader>
);

export default SegmentContentLoader;
