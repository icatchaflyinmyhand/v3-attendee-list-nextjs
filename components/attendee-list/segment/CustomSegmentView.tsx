import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown } from 'react-bootstrap';

import DefaultSegmentView from './DefaultSegmentView';
import SegmentRenameBtn from './RenameBtn';
import SegmentDeleteBtn from './DeleteBtn';

const CustomSegmentView = ({
  segment,
  className,
  onClick,
  disabled,
}) => (
  <Dropdown
    id={segment.name}
    className={className}
    drop={"right"}
  >
    <DefaultSegmentView
      segment={segment}
      className="segment-btn"
      onClick={onClick}
      disabled={disabled}
    />
    <Dropdown.Toggle />
    <Dropdown.Menu>
      <SegmentRenameBtn
        segment={segment}
      />
      <SegmentDeleteBtn
        segment={segment}
      />
    </Dropdown.Menu>
  </Dropdown>
);

CustomSegmentView.propTypes = {
  segment: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    kind: PropTypes.string.isRequired,
    attendees_count: PropTypes.number.isRequired,
  }).isRequired,
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default CustomSegmentView;
