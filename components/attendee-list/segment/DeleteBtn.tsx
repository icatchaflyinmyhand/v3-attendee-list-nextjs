import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Button,
  FormControl,
  Dropdown,
  Modal,
} from 'react-bootstrap'

import FadeAlert from 'components/FadeAlert'

const DeleteBtn = ({ segment, setAlertMsgs }) => {
  const [showDeleteSegmentModal, setShowDeleteSegmentModal] = useState(false)
  const [processing, setProcessing] = useState(false)
  const [alertMsg, setAlertMsg] = useState(null)
  const [alertType, setAlertType] = useState('info')

  const handleDeleteBtnOnClick = () => {
    setProcessing(true);
    // TODO
    console.log('deleted!')
  }
  
  return (
    <>
      <Dropdown.Item onClick={() => setShowDeleteSegmentModal(true)}>
        { ('attendee_segments.delete_segment') }
      </Dropdown.Item>
      <Modal
        id='delete-segment-modal'
        show={showDeleteSegmentModal}
        onHide={() => setShowDeleteSegmentModal(false)}
        backdrop='static'
      >
        <Modal.Header>
          <Modal.Title>
            { ('attendee_segments.delete_segment') }
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FadeAlert
            msg={alertMsg}
            type={alertType}
            handleAlertDismiss={() => setAlertMsg(null)}
          />
          { ('attendee_segments.delete_confirmation') }
          <span className='segment-name'>
            { " '#{@props.segment.name}'" }
          </span>
          <span>?</span>
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => setShowDeleteSegmentModal(false)}
            disabled={processing}
          >
            { ('general.cancel') }
          </Button>
          <Button
            variant='danger'
            onClick={handleDeleteBtnOnClick}
            disabled={processing}
          >
            {
              processing ? ('general.processing') : ('general.delete')
            }
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

DeleteBtn.propTypes = {
  segment: PropTypes.object.isRequired,
  setAlertMsgs: PropTypes.func.isRequired,
}

export default DeleteBtn
