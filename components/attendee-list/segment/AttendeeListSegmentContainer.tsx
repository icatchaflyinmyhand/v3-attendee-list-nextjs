import React, { useState, useEffect } from 'react';
import {
  DropdownButton,
  Dropdown,
} from 'react-bootstrap';
import ContentLoader from 'react-content-loader';

import SegmentContentLoader from 'components/attendee-list/segment/SegmentContentLoader'
import SegmentItem from 'components/attendee-list/segment/SegmentItem'

const firstLoading = true;
const priorityItems = [{}];
const segments = [{id: 1},{id: 2}];

const AttendeeListSegmentContainer = (params) => {
  return(
    <div id="attendee-list-segments">
      <div className="navigation">
        <div className="navigation-list">
          {
            (firstLoading) && (
              [...Array(4).keys()].map((index) =>
                <SegmentContentLoader key={index} />
            ))
          }
          {
            (!firstLoading && priorityItems && segments.length > 0) && (
              segments.map((segment) =>
                <SegmentItem
                  key={segment.id}
                  // segment={segment}
                  // infiniteLoading={@props.infiniteLoading}
                  // onClick={@_handleSegmentItemOnClick}
                />
            ))
          }
        </div>
        {/* {
          if !@props.firstLoading && (moreSegments = @state.moreItems) && moreSegments.length > 0
            <div ref='moreDropdown' className='more-segments'>
              {
                activeSegmentIndex = moreSegments.findIndex (segment) => @_isSegmentActive(segment)
                if (activeSegment = @context.activeSegment) && activeSegmentIndex > -1
                  <SegmentItem
                    key={activeSegment.id}
                    segment={activeSegment}
                    infiniteLoading={@props.infiniteLoading}
                    onClick={@_handleSegmentItemOnClick}
                  />
              }
              {
                moreSegments = moreSegments.filter (segment) => !@_isSegmentActive(segment)
                if moreSegments.length > 0
                  <DropdownButton
                    id='more-segments'
                    className='segment'
                    title={@_moreBtnTitle(moreSegments.length)}
                    pullRight
                  >
                    {
                      moreSegments.map (segment) =>
                        <OverlayTooltip
                          key={segment.id}
                          tooltip={segment.tooltip}
                        >
                          <Dropdown.Item onClick={() => @_handleSegmentItemOnClick(segment.id)}>
                            <span className='segment-title'>
                              { segment.name }
                            </span>
                            <span className='attendees-count'>
                              { "(#{segment.attendees_count})" }
                            </span>
                          </Dropdown.Item>
                        </OverlayTooltip>
                    }
                  </DropdownButton>
              }
            </div>
        } */}
      </div>
    </div>
  )
}

export default AttendeeListSegmentContainer;
