import React from 'react';
import PropTypes from 'prop-types';

import CustomSegmentView from './CustomSegmentView';
import DefaultSegmentView from './DefaultSegmentView';

// interface Props {
//   segment: {
//     id: number,
//     name: string,
//     kind: string,
//     attendeeCount: number,
//   }
// }

const SegmentItem = ({ segment, infiniteLoading, onClick }) => {
  const isSegmentActive = () => {
    const { id }  = segment;
    // const { activeSegment: { id: activeSegmentId } } = context;
    const activeSegmentId = 123;
    return activeSegmentId === id;
  }
  const tabItemActiveClass = () => {
    return `segment ${isSegmentActive() ? 'active' : ''}`;
  }
  const handleSegmentItemOnClick = () => {
    const { id } = segment;
    onClick(id);
  }
  const disabled = isSegmentActive() || infiniteLoading;

  if (segment.kind === 'custom') {
    return (
      <CustomSegmentView
        segment={segment}
        className={tabItemActiveClass()}
        onClick={handleSegmentItemOnClick}
        disabled={disabled}
      />
    )
  }
  return (
    <DefaultSegmentView
      segment={segment}
      className={tabItemActiveClass()}
      onClick={handleSegmentItemOnClick}
      disabled={disabled}
    />
  )
}

SegmentItem.propTypes = {
  segment: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    kind: PropTypes.string.isRequired,
    attendeeCount: PropTypes.number.isRequired,
  }).isRequired,
  infiniteLoading: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

SegmentItem.defaultProps = {
  infiniteLoading: false,
};

export default SegmentItem
