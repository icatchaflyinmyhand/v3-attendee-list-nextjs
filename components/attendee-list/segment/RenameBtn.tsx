import React, { useState } from 'react'
import PropTypes from 'prop-types'
import FadeAlert from 'components/FadeAlert'
import {
  Button,
  FormControl,
  Dropdown,
  Modal,
} from 'react-bootstrap'

const RenameBtn = ({ segment, setAlertMsgs }) => {
  const [showRenameSegmentModal, setShowRenameSegmentModal] = useState(false);
  const [newSegment, setNewSegment] = useState(segment)
  const [processing, setProcessing] = useState(false)
  const [alertMsg, setAlertMsg] = useState(undefined)
  const [alertType, setAlertType] = useState('info')

  const handleSegmentNameChange = (event) => {
    const temp = newSegment;
    temp.name = event.target.value
    setNewSegment({temp});
  }
  const handleSaveBtnOnClick = () => {
    setProcessing(true)
    //TODO
    console.log('saved!');
  }

  const handleInputOnKeyDown = (event) => {
    if (event.keyCode === 13) handleSaveBtnOnClick();
  }
  
  const shouldDisableBtn = () => (
    newSegment?.name?.length === 0 || processing
  )

  return (
    <>
      <Dropdown.Item onClick={() => setShowRenameSegmentModal(true)}>
          { ('attendee_segments.rename_segment') }
      </Dropdown.Item>
      <Modal
        id='rename-segment-modal'
        show={showRenameSegmentModal}
        onHide={() => setShowRenameSegmentModal(false)}
        backdrop='static'
      >
        <Modal.Header>
          <Modal.Title>
            { ('attendee_segments.rename_segment') }
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <FadeAlert
            msg={alertMsg}
            type={alertType}
            handleAlertDismiss={() => setAlertMsg(null)}
          />
          { ('attendee_segments.segment_name') }
          <FormControl
            type="text"
            name='segment[name]'
            value={newSegment.name}
            onChange={handleSegmentNameChange}
            onKeyDown={handleInputOnKeyDown}
            placeholder={('attendee_segments.segment_name')}
            autoFocus
          />
        </Modal.Body>
        <Modal.Footer>
          <Button
            onClick={() => setShowRenameSegmentModal(false)}
            disabled={processing}
          >
            { ('general.cancel') }
          </Button>
          <Button
            variant='danger'
            onClick={handleSaveBtnOnClick}
            disabled={shouldDisableBtn()}
          >
            {
              processing ? ('general.processing') : ('general.save')
            }
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

RenameBtn.propTypes = {
  segment: PropTypes.object.isRequired,
  setAlertMsgs: PropTypes.func.isRequired,
}

export default RenameBtn
