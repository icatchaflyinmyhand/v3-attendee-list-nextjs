import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button } from 'react-bootstrap';

import OverlayTooltip from 'components/OverlayToolTip';
import SegmentTitle from './SegmentTitle';

const OverlayButtonWrapper = styled.div`
  display: inline-block;
  cursor: not-allowed;
  height: 100%;
`;

const StyledOverlayButton = styled(Button)`
  pointer-events: ${((props) => (props.disabled && 'none'))};
`;

const DefaultSegmentView = ({
  segment: {
    name,
    attendees_count: attendeesCount,
    tooltip,
  },
  className,
  onClick,
  disabled
}) => (
  <OverlayTooltip
    placement="bottom"
    tooltip={tooltip}
  >
    <OverlayButtonWrapper>
      <StyledOverlayButton
        className={className}
        onClick={onClick}
        disabled={disabled}
      >
        <SegmentTitle
          name={name}
          attendeesCount={attendeesCount}
        />
      </StyledOverlayButton>
    </OverlayButtonWrapper>
  </OverlayTooltip>
)

DefaultSegmentView.propTypes = {
  segment: PropTypes.shape({
    name: PropTypes.string.isRequired,
    attendees_count: PropTypes.number.isRequired,
    tooltip: PropTypes.string.isRequired,
  }).isRequired,
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
}

export default DefaultSegmentView
