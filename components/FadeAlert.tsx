import React from 'react'
import PropTypes from 'prop-types'
import {
  Alert,
  Fade,
} from 'react-bootstrap'

const style = {
  msg: {
    fontSize: '14px'
  }
}

const FadeAlert = ({ msg, handleAlertDismiss, type }) => {
  return (
    <Fade
      in={msg.length > 0}
      mountOnEnter
      unmountOnExit
    >
      <Alert
        className='fade-alert'
        variant={type}
        onClose={handleAlertDismiss}
      >
        <div style={style.msg}>
          { msg }
        </div>
      </Alert>
    </Fade>
  )
}

FadeAlert.propTypes = {
  msg: PropTypes.string,
  type: PropTypes.string,
  handleAlertDismiss: PropTypes.func.isRequired,
}

FadeAlert.defaultProps = {
  msg: undefined,
  type: 'info',
}

export default FadeAlert
