import React from 'react'
import PropTypes from 'prop-types'
import {
  OverlayTrigger,
  Tooltip,
} from 'react-bootstrap'

const OverlayToolTip = ({placement, tooltip, children}) => {
  const renderTooltip = () => (
    <Tooltip id="tooltip">
      { tooltip }
    </Tooltip>
  )
  return (
    <OverlayTrigger
      placement={placement || "right"}
      overlay={renderTooltip()}
      rootClose
      trigger="hover"
    >
      { children }
    </OverlayTrigger>
  )
}

OverlayToolTip.propTypes = {
  placement: PropTypes.string.isRequired,
  tooltip: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}

export default OverlayToolTip
