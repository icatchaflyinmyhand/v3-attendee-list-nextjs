import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types'

import { selectAttendeeById, Attendee, AttendeeFetchedLevel } from 'store/models/attendee';

const AttendeeProfile = ({
  attendeeId,
}: {
  attendeeId: string,
}) => {
  const attendee: Attendee = useSelector((state) => selectAttendeeById(state, attendeeId));

  useEffect(() => {
    //todo if attendee fetched level is preview, should fetch detail info
  }, []);

  if (!attendee) {
    return null;
  }

  return (
    <div>
      <span>
        {attendee.id}
      </span>
      <span>
        {attendee.name}
      </span>
      <span>
        {attendee.email}
      </span>
      <span>
        {attendee.company}
      </span>
    </div>
  )
};

AttendeeProfile.propTypes = {
  attendeeId: PropTypes.string.isRequired,
}

export default AttendeeProfile;
